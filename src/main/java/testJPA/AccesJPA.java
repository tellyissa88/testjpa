package testJPA;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class AccesJPA<T> {
	private EntityManager em;
	
	private Logger logger;

	public AccesJPA() {
		EntityManagerFactory emf = null;
		emf = Persistence.createEntityManagerFactory("testJPA");
		em = emf.createEntityManager();
		logger = Logger.getLogger("logger");
	}
	/**
	 * Lire tout les enreg de la base de donnees
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PersonnePOJO> lireTous() {
		return em.createQuery("SELECT p FROM PersonnePOJO p").getResultList();
		// return em.createNamedQuery("findAll").getResultList();

	}
	/**
	 * L'utilisation de la classe EntityManager pour la cr�ation d'une occurrence
	 */
	public void ajouter(PersonnePOJO p) {
		// p.setId(lireDernierId()+1);
		if (p != null) {
			em.getTransaction().begin();
			em.persist(p);
			em.getTransaction().commit();
		} else {
			logger.log(Level.INFO, "Personne non trouv�e");
		}
	}

	/**
	 * Permet de modifier un enreg dans la base de donnee
	 * 
	 * @param cle la cle de l'enreg a modifier
	 * @param pjp l'objet a mettre en place
	 */
	public void Modifier(int cle, PersonnePOJO pjp) {
		PersonnePOJO pj = null;
		pj = em.find(PersonnePOJO.class, cle);
		if (pjp != null) {
			em.getTransaction().begin();
			pj.setPrenom(pjp.getPrenom());
			pj.setNom(pjp.getNom());
			pj.setTelephone(pjp.getTelephone());
			em.merge(pj);
			em.getTransaction().commit();
		} else {
			System.out.println("Personne non trouv�e");

		}
	}

	/**
	 * Supprimer une personne
	 * 
	 * @param cle, qui permet de supprimer la personne {@link PersonnePOJO}
	 */
	public void effacer(int cle) {
		PersonnePOJO personne = null;
		try {
			personne = em.getReference(PersonnePOJO.class, cle);
			em.getTransaction().begin();
			em.remove(personne);
			em.getTransaction().commit();
		} catch (Exception e) {
			logger.log(Level.INFO, "la personne est introuvable, veuillez entrer une valid�");
		}
	}
	/**
	 * L'utilisation de la classe EntityManager pour rechercher des occurrences
	 */

	public PersonnePOJO chercher(int cle) {
		PersonnePOJO personne = null;
		try {
			personne = em.getReference(PersonnePOJO.class, cle);
			logger.log(Level.INFO, personne.toString());

		} catch (Exception e) {
			logger.log(Level.INFO, "personne introuvable");
		}
		return personne;
	}

	/**
	 * Recupere le dernier id de la table personne
	 * 
	 * @return
	 */
	public int lireDernierId() {
		return (int) em.createQuery("SELECT MAX(p.id) FROM PersonnePOJO p").getSingleResult();
	}

	/**
	 * Counter le nombre d'enreg de la table personne
	 * 
	 * @return
	 */
	public int lireNombreEnreg() {
		return (int) em.createQuery("SELECT COUNT(p.id) FROM PersonnePOJO p group by p.id").getResultList().size();
	}
	/**
	 * Pour lire un enregistrement
	 * 
	 * @param cle identfiant
	 * @return l'objet retourne
	 */
	public PersonnePOJO lire(int cle) {
		return (PersonnePOJO) em.createNamedQuery("PersonnePOJO.lire").setParameter("id", cle).getSingleResult();
	}
	
	public static void main(String[] args) {
		AccesJPA jp = null;
		jp = new AccesJPA();
		// System.out.println(jp.lireDernierId());
		// System.out.println(pd.liste().toString());
		System.out.println(jp.lire(1));
		// System.out.println(jp.lire(2).toString());
		// jp.ajouter();
		// jp.chercher();
//		PersonnePOJO p = null;
//		p = new PersonnePOJO();
//		p.setPrenom("modif");
//		p.setNom("nommodi");
//		p.setTelephone("88");
		// jp.Modifier(50,p);

		// System.out.println(jp.getPersonnePOJO(5));
	}

}
