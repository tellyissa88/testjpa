package testJPA;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


/**
 * The persistent class for the personne database table.
 * 
 */
@Entity
@Table(name="personne")
@NamedQueries({@NamedQuery(name="PersonnePOJO.findAll", query="SELECT p FROM PersonnePOJO p"),
	@NamedQuery(name="PersonnePOJO.lire", query="SELECT p FROM PersonnePOJO p WHERE p.id = :id")})
public class PersonnePOJO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int id;

	private String nom;

	private String prenom;

	private String telephone; 
	
	@OneToOne
	@JoinColumn(name="idadresse") 
	private AdressePOJO adresse;
	
	@Transient private String description;
	public PersonnePOJO() {
	}
	
	public PersonnePOJO(String nom, String prenom, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}
	

	public AdressePOJO getAdresse() {
		return adresse;
	}

	public void setAdresse(AdressePOJO adresse) {
		this.adresse = adresse;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "PersonnePOJO [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", telephone=" + telephone
				+ ", adresse=" + adresse + ", description=" + description + "]\n";
	}
}