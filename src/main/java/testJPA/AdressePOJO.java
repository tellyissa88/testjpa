package testJPA;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the adresse database table.
 * 
 */ 
@Entity
@Table(name="adresse")
@NamedQueries({@NamedQuery(name="AdressePOJO.findAll", query="SELECT a FROM AdressePOJO a"),
	@NamedQuery(name="AdressePOJO.lire", query="SELECT a FROM PersonnePOJO a WHERE a.id = :id")})

public class AdressePOJO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String codePostal;

	private String rue;

	private String ville;

	public AdressePOJO() {
	}

	public int getId() {
		return this.id;
	}
	

	public AdressePOJO(int id, String codePostal, String rue, String ville) {
		super();
		this.id = id;
		this.codePostal = codePostal;
		this.rue = rue;
		this.ville = ville;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodePostal() {
		return this.codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getRue() {
		return this.rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return this.ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return "AdressePOJO [id=" + id + ", codePostal=" + codePostal + ", rue=" + rue + ", ville=" + ville + "]";
	}
	

}