package testJPA;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class testJPA {
	static AccesJPA jpa;
	private EntityManager em;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		jpa = new AccesJPA();
	}
	
//	@Test
//    void Ajouter() {
//		PersonnePOJO p = null;
//		p = new PersonnePOJO();
//		p.setId(jpa.lireDernierId()+1);
//		p.setNom("liammmm");
//		p.setPrenom("camara");
//		p.setTelephone("80975687");
//		jpa.ajouter(p);
//		assertEquals("liammmm", p.getNom());
//		assertNotNull(p.getPrenom());
//		assertNotEquals("0504789869", p.getTelephone());
//	}
	
	@Test
    void Modifier() {
		PersonnePOJO p = null;
		p = new PersonnePOJO();
		p.setNom("kadiatou");
		p.setPrenom("Barry");
		p.setTelephone("0758987456");
		List<PersonnePOJO> res = null;
		res = jpa.lireTous();
		jpa.Modifier(2,p);
		//List<PersonnePOJO> res1 = null;
		//res1 = jpa.lireTous();
		//PersonnePOJO pjo = res.get(res.size()-1);
		assertEquals("kadiatou", p.getNom());
		assertNotEquals("Marie", p.getNom());
	}	
	@Test
	void testEffacer() {
		jpa.effacer(4);
		
	}
	@Test
    void testLireTous() {
		List<PersonnePOJO> res = null;
		res = jpa.lireTous();
		assertEquals(jpa.lireNombreEnreg(),res.size());
		System.out.println(jpa.lireNombreEnreg());
	}
}
